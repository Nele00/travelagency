//CAROUSEL
const carouselSlide = document.querySelector('.carouselSlide');
const carouselImages = document.querySelectorAll('.carouselSlide img');

const prevBtn = document.querySelector('#previous');
const nextBtn = document.querySelector('#next');

let counter = 0;
const size = carouselImages[0].clientWidth;

carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';

nextBtn.addEventListener('click', () => {
    if (counter >= carouselImages.length - 1) return;
    carouselSlide.style.transition = 'transform 0.4s ease-in-out';
    counter++;
    carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
});

prevBtn.addEventListener('click', () => {
    if (counter <= 0) return;
    carouselSlide.style.transition = 'transform 0.4s ease-in-out';
    counter--;
    carouselSlide.style.transform = 'translateX(' + (-size * counter) + 'px)';
});

//OFFERS
let offers = [{
        "id": 1,
        "city": "London",
        "price": 59,
        "description": "London is a capital city of United Kingdom. It is very beautifull place!"
    },
    {
        "id": 2,
        "city": "Hamburg",
        "price": 79,
        "description": "Hamburg is one of the largest cities in northern Germany!"
    },
    {
        "id": 3,
        "city": "Alicante",
        "price": 89,
        "description": "Alicante is perfect place in Spain to rest yourself on the beach!"
    },
    {
        "id": 4,
        "city": "Milano",
        "price": 119,
        "description": "If you like fashion, Milano is perfect place for you to visit!"
    },
]


//SERVICES TABS
const tabs = document.querySelectorAll('[data-tab-target]')
const tabContents = document.querySelectorAll('[data-tab-content]')

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        const target = document.querySelector(tab.dataset.tabTarget)
        tabContents.forEach(tabContent => {
            tabContent.classList.remove('active')
        })
        tabs.forEach(tab => {
            tab.classList.remove('active')
        })
        tab.classList.add('active')
        target.classList.add('active')
    })
});

//VALIDATE EMAIL
const email = document.querySelector('.email');
const update = document.querySelector('.status');

email.addEventListener('input', inputEmail);

function inputEmail(e) {
    const input = e.target.value;
    if (input && /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i.test(input)) {
        update.textContent = 'Valid Email!';
        update.classList.add('success');
        update.classList.remove('failure');
    } else {
        update.textContent = 'Keep Going...';
        update.classList.remove('success');
        update.classList.add('failure');
    }
    if (input === "") {
        update.textContent = 'Enter your email address';
        update.classList.remove('success');
        update.classList.remove('failure');

    }
};